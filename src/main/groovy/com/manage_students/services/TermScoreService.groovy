package com.manage_students.services

import com.manage_students.models.TermScore
import com.manage_students.repositories.TermScoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

import javax.persistence.EntityNotFoundException

@Service
@CacheConfig(cacheNames = ["term-score"])
class TermScoreService{
    @Autowired
    TermScoreRepository termScoreRepository

    @Cacheable
    Page findAll(Integer pageNo, Integer pageSize){
        Pageable pageable = PageRequest.of(pageNo, pageSize)
        termScoreRepository.findAll(pageable)
    }

    TermScore findById(Long id){
        termScoreRepository.findById(id).orElse(null)
    }

    TermScore findByIdOrError(Long id){
        termScoreRepository.findById(id).orElseThrow({
            new EntityNotFoundException()
        })
    }

    TermScore save(TermScore termScore){
        termScoreRepository.save(termScore)
    }

    TermScore update(TermScore termScore, Long id){
        def persisted = findByIdOrError(id)
        persisted.with {
            term = persisted.term
            score = persisted.score
        }
        termScoreRepository.save(persisted)
    }

    TermScore deleteById(Long id){
        def persisted = findByIdOrError(id)
        termScoreRepository.delete(persisted)
        persisted
    }

    
}
