package com.manage_students.repositories

import com.manage_students.models.Student
import org.springframework.data.repository.PagingAndSortingRepository

interface StudentRepository extends PagingAndSortingRepository<Student, Long> {}
