package com.manage_students.controllers

import com.manage_students.models.Student
import com.manage_students.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.*

import javax.transaction.Transactional

@RestController
@RequestMapping('students')
@Transactional
class StudentController {
    @Autowired
    StudentService studentService

    @GetMapping('')
    Page findAll(@RequestParam(defaultValue = "0") Integer pageNo,
                 @RequestParam(defaultValue = "100") Integer pageSize) {
        studentService.findAll(pageNo, pageSize)
    }

    @GetMapping('{id}')
    Student findOne(@PathVariable Long id) {
        studentService.findById(id)
    }

    @PostMapping('')
    Student save(@RequestBody Student student) {
        studentService.save(student)
    }

    @PutMapping('{id}')
    Student update(@RequestBody Student student, @PathVariable Long id) {
        studentService.update(student, id)
    }

    @DeleteMapping('{id}')
    Student deleteById(@PathVariable Long id) {
        studentService.deleteById(id)
    }
}