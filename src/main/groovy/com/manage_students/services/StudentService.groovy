package com.manage_students.services

import com.manage_students.models.Student
import com.manage_students.repositories.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

import org.springframework.cache.annotation.Cacheable
import javax.persistence.EntityNotFoundException

@Service
@CacheConfig(cacheNames = ["student"])
class StudentService{
    @Autowired
    StudentRepository studentRepository

    @Cacheable
    Page findAll(Integer pageNo, Integer pageSize){
        Pageable pageable = PageRequest.of(pageNo, pageSize)
        studentRepository.findAll(pageable)
    }

    Student findById(Long id){
        studentRepository.findById(id).orElse(null)
    }

    Student findByIdOrError(Long id){
        studentRepository.findById(id).orElseThrow({
            new EntityNotFoundException()
        })
    }

    Student save(Student student){
        student.termScores?.each {
            it.student = student
        }
        studentRepository.save(student)
    }

    Student update(Student student, Long id){
        def persisted = findByIdOrError(id)
        persisted.with {
            first_name = student.first_name
            last_name = student.last_name
            sex = student.sex
            date_of_birth = student.date_of_birth
            address = student.address
        }
        def toBeRemoved = []
        persisted.termScores.each {
            def a = student.termScores.find{it2 -> it2.id == it.id}
            if (a==null) toBeRemoved.add(it)
            else it.student = a.student
        }
        persisted.termScores.removeAll(toBeRemoved)
        student.termScores.each {
            if (it.id == null){
                it.student = persisted
                persisted.termScores.add(it)
            }
        }
        studentRepository.save(persisted)
    }

    Student deleteById(Long id){
        def student = findByIdOrError(id)
        studentRepository.delete(student)
        student
    }

}
