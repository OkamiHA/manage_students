package com.manage_students

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class ManageStudentsApplicationTests {

	@Test
	void contextLoads() {
	}

}
