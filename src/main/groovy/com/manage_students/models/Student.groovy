package com.manage_students.models


import org.springframework.format.annotation.DateTimeFormat

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
class Student{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    String first_name

    String last_name

    String sex

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date date_of_birth

    String address

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,
    orphanRemoval = true,
    mappedBy = 'student')
    List<TermScore> termScores

}