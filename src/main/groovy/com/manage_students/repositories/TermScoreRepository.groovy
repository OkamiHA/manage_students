package com.manage_students.repositories

import com.manage_students.models.TermScore
import org.springframework.data.repository.PagingAndSortingRepository

interface TermScoreRepository extends PagingAndSortingRepository<TermScore, Long> {}