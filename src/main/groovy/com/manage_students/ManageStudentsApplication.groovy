package com.manage_students

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication
@EnableCaching
class ManageStudentsApplication {

	static void main(String[] args) {
		SpringApplication.run(ManageStudentsApplication, args)
	}

}
