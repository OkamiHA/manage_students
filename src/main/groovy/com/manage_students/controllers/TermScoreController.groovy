package com.manage_students.controllers

import com.manage_students.models.Student
import com.manage_students.models.TermScore
import com.manage_students.services.TermScoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.*

import javax.transaction.Transactional

@RestController
@RequestMapping("term-scores")
@Transactional
class TermScoreController{
    @Autowired
    TermScoreService termScoreService

    @GetMapping("")
    Page findAll(@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "1000") Integer pageSize){
        termScoreService.findAll(pageNo, pageSize)
    }

    @GetMapping('{id}')
    TermScore findOne(@PathVariable Long id) {
        termScoreService.findById(id)
    }

    @PostMapping('')
    TermScore save(@RequestBody TermScore termScore) {
        termScoreService.save(termScore)
    }

    @PutMapping('{id}')
    TermScore update(@RequestBody TermScore termScore, @PathVariable Long id) {
        termScoreService.update(termScore, id)
    }

    @DeleteMapping('{id}')
    TermScore deleteById(@PathVariable Long id) {
        termScoreService.deleteById(id)
    }
}

