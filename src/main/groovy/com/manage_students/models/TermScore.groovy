package com.manage_students.models

import com.fasterxml.jackson.annotation.JsonIgnore

import javax.persistence.*

@Entity
class TermScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnore
    Student student

    String term

    Float score
}
